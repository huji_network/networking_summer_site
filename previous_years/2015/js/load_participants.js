var data = {
    1 :  {  "name" : "Janos Tapolcai",
            "company" : "BME",
            "company_link" : "http://www.bme.hu/",
            "email" : "tapolcai@tmit.bme.hu",
            "pic" : "http://lendulet.tmit.bme.hu/lendulet_website/SVN/CVs/tapolcai/cv.png",
            "content" : "",
            "website" : "http://lendulet.tmit.bme.hu/lendulet_website/?page_id=291"},

    2 : {  "name" : "Gabor Retvari",
            "company" : "BME",
            "company_link" : "http://www.bme.hu/",
            "email" : "retvari@tmit.bme.hu",
            "pic" : "http://lendulet.tmit.bme.hu/lendulet_website/SVN/CVs/retvari/cv.png",
            "content" : "",
            "website" : "http://lendulet.tmit.bme.hu/lendulet_website/?page_id=320"},

    3 :  {  "name" : "Costin Raiciu",
            "company" : "Universitatea Politehnica",
            "company_link" : "http://www.upb.ro/en/",
            "email" : "costin.raiciu@cs.pub.ro",
            "pic" : "http://nets.cs.pub.ro/~costin/pics/poza.png",
            "content" : "",
            "website" : "http://nets.cs.pub.ro/~costin/"},

    4 : {  "name" : "Eytan Modiano",
            "company" : "MIT",
            "company_link" : "http://web.mit.edu/",
            "email" : "modiano@mit.edu",
            "pic" : "https://modiano.mit.edu/sites/default/files/images/modiano.jpg",
            "content" : "",
            "website" : "https://modiano.mit.edu/"},

    5 :  {  "name" : "Sharon Goldberg",
            "company" : "Boston University",
            "company_link" : "http://www.bu.edu/",
            "pic" : "https://www.cs.bu.edu/~goldbe/sharon6.jpg",
            "content" : "",
            "website" : "https://www.cs.bu.edu/~goldbe/"},

    6 : {  "name" : "Phillipa Gill",
            "company" : "Stony Brook University",
            "company_link" : "http://www.stonybrook.edu/",
            "pic" : "http://www3.cs.stonybrook.edu/~phillipa/img/pgill.JPG",
            "content" : "",
            "website" : "http://www3.cs.stonybrook.edu/~phillipa/"},

    7 :  {  "name" : "Constantine Dovrolis",
            "company" : "Georgia Tech",
            "company_link" : "http://www.gatech.edu/",
            "email" : "constantine@gatech.edu",
            "pic" : "http://www.cc.gatech.edu/fac/Constantinos.Dovrolis/Images/const-caricature.png",
            "content" : "",
            "website" : "http://www.cc.gatech.edu/fac/Constantinos.Dovrolis/"},

    8 : {  "name" : "Mike Dinitz",
            "company" : "Johns Hopkins University",
            "company_link" : "https://www.jhu.edu/",
            "email" : "mdinitz@cs.jhu.edu",
            "pic" : "http://www.cs.jhu.edu/~mdinitz/images/mike_catit2013.jpg",
            "content" : "",
            "website" : "http://www.cs.jhu.edu/~mdinitz/"},

    9 :  {  "name" : "David Choffnes",
            "company" : "Northeastern University",
            "company_link" : "http://www.northeastern.edu/",
            "email" : " choffnes@ccs.neu.edu",
            "pic" : "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/4/005/046/3f7/2edaed6.jpg",
            "content" : "",
            "website" : "http://david.choffnes.com/"},

    10 : {  "name" : "Hitesh Ballani",
            "company" : "Microsoft Research",
            "company_link" : "http://research.microsoft.com/en-us/labs/Cambridge/",
            "email" : "hitesh.ballani@microsoft.com",
            "pic" : "http://research.microsoft.com/en-us/um/people/hiballan/img/hb-jul13-small.jpg",
            "content" : "",
            "website" : "http://research.microsoft.com/en-us/um/people/hiballan/"},

    11 :  {  "name" : "Laurent Vanbever",
            "company" : "ETH Zurich",
            "company_link" : "https://www.ethz.ch/en.html",
            "email" : "lvanbever@ethz.ch",
            "pic" : "http://www.ee.ethz.ch/news-and-events/d-itet-news-channel/2015/01/new-at-d-itet-prof-laurent-vanbever/_jcr_content/news_content/textimage/image.imageformat.lightbox.1399072167.png",
            "content" : "",
            "website" : "http://vanbever.eu/"},

    12 : {  "name" : "Gill Zussman",
            "company" : "Columbia University",
            "company_link" : "http://www.columbia.edu/",
            "email" : " gil@ee.columbia.edu",
            "pic" : "http://wimnet.ee.columbia.edu/wp-content/uploads/2013/04/people-zussman.jpg",
            "content" : "",
            "website" : "http://wimnet.ee.columbia.edu/people/gil-zussman/"},

    13 : {  "name" : "Aaron D. Jaggard",
            "company" : "Naval Research Laboratory",
            "company_link" : "http://www.nrl.navy.mil/",
            "email" : " adj@dimacs.rutgers.edu",
            "pic" : "http://www.dimacs.rutgers.edu/~adj/adj-small.jpg",
            "content" : "",
            "website" : "http://www.dimacs.rutgers.edu/~adj/"},

    14 : {  "name" : "Jonathan Perry",
            "company" : "MIT",
            "company_link" : "http://web.mit.edu/",
            "email" : " yonch@mit.edu",
            "pic" : "http://www.yonch.com/wp-content/uploads/2013/01/portrait_sm2-e1429765821320.jpg",
            "content" : "",
            "website" : "http://www.yonch.com/"}

};

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var sortObjectByKey = function(obj){
    var keys = [];
    var sorted_obj = {};

    for(var key in obj){
        if(obj.hasOwnProperty(key)){
            name_arr = obj[key].name.split(" ");
            keys.push([name_arr[name_arr.length-1], key]);
        }
    }
    // sort keys
    keys.sort();
    var ind = 1;
    // create new array based on Sorted Keys
    jQuery.each(keys, function(i, key){
        sorted_obj[ind] = obj[key[1]];
        ind += 1;
    });
    return sorted_obj;
};

$(document).ready(function(){
  var sorted_data = sortObjectByKey(data);
  var num_participants = Object.size(data);
  var people_per_row = 4;
  var num_rows = num_participants/people_per_row;
  total_html = "";
  for(row=0; row< num_rows ; row++){
    var row_html = "<div class=\"row center\">";
    for(col = 1; col <= people_per_row ; col++){
      if(row*people_per_row + col > num_participants){
        continue;
      }
      row_html +=
        "<div class=\"col s3\">"+
        "<div class=\"card\">"+
          "<div class=\"card-image\">"+
            "<img src=\"" + sorted_data[row*people_per_row + col].pic +  "\" width=\"280\" height=\"240\">"+
              "<span class=\"card-title\">" + sorted_data[row*people_per_row + col].name +  "</span>"+
            "</div>"+
            "<div class=\"card-content\">"+
              "<p><a href=\""+ sorted_data[row*people_per_row + col].company_link +"\">" + sorted_data[row*people_per_row + col].company + "</a></p>"+
            "</div>"+
            "<div class=\"center card-action\">"+
              "<a href=\"" + sorted_data[row*people_per_row + col].website +  "\">Website</a>"+
            "</div>"+
          "</div>"+
        "</div>";
    }
    row_html += "</div>"
    total_html += row_html;
  }
  $("#people").append(total_html);
});

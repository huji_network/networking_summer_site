function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1)});
}

var data = {

    1 : {  "name" : "Costin Raiciu",
            "company" : "University Politehnica of Bucharest",
            "short_company" : "UPB",
            "company_link" : "https://cs.pub.ro/",
            "pic" : "https://cs.pub.ro/images/comprofiler/350_52b9a08299fcc.jpg",
            "content" : "",
            "talk_title" : "SymNet: Scalable Symbolic Execution For Modern Networks",
            "talk_abstract" : "We present SymNet, a network static analysis tool based on symbolic execution. SymNet injects symbolic packets and tracks their evolution through the network. Our key novelty is SEFL, a language we designed for expressing data plane processing in a symbolic-execution friendly manner. SymNet statically analyzes an abstract data plane model that consists of the SEFL code for every node and the links between nodes. SymNet can check networks containing routers with hundreds of thousands of prefixes and NATs in seconds, while verifying packet header memory-safety and covering network functionality such as dynamic tunneling, stateful processing and encryption. We used SymNet to debug mid- dlebox interactions from the literature, to check properties of our department’s network and the Stanford backbone.<br><br>Modeling network functionality is not easy. To aid users we have developed parsers that automatically generate SEFL models from router and switch tables, firewall configura- tions and arbitrary Click modular router configurations. The parsers rely on prebuilt models that are exact and fast to an- alyze. Finally, we have built an automated testing tool that combines symbolic execution and testing to check whether the model is an accurate representation of the real code.",
            "bio" : "Costin Raiciu received a PhD from University College London in 2011 under the supervision of Mark Handley and David Rosenblum. Since then he moved to  University Politehnica of Bucharest where he is Associaote Professor. Costin’s past work has largely focused on developing the Multipath TCP protocol and congestion control, as well as standardising it. Presently, he leads a research group in Bucharest that has been focusing on two main research directions:<br> - understanding the effects the ongoing Multipath TCP deployment (in Apple phones) will have on the rest of the Internet, if Multipath TCP is widely adopted. See two papers in NSDI 2015 on this topic.<br> - network verification. We have been developing SymNet (Sigcomm 2016), a symbolic execution tool for networks, and have shown how it can be used to allow Internet innovation (Eurosys 2015).",
            "website" : "https://cs.pub.ro/index.php/people/userprofile/costin_raiciu",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "16:15:00",
            "time_end" : "16:55:00"
          },

    2 : {  "name" : "Phillipa Gill",
            "company" : "University of Massachusetts -- Amherst",
            "short_company" : "UMASS Amherst",
            "company_link" : "https://www.cics.umass.edu/",
            "pic" : "http://www3.cs.stonybrook.edu/~phillipa/img/pgill.JPG",
            "content" : "",
            "talk_title" : "Measuring And Circumventing Internet Censorship (I)",
            "talk_abstract" : "This tutorial will be split into two parts. The first half of the tutorial will cover how censorship is implemented at different layers of the protocol stack. For each layer, we will discuss how censorship is implemented, what might trigger censorship, and how we can design experiments to better understand censorship. The second half of the tutorial will focus on research going on in my research group. This will include results of recent field studies undertaken with the ICLab platform, methods to detect more subtle traffic differentiation. Time permitting, I will also discuss our recent work on AS-aware Tor clients and techniques to circumvent traffic shaping middle boxes.",
            "bio" : "Phillipa Gill is an assistant professor in the Computer Science Department at the University of Massachusetts -- Amherst. Her work focuses on many aspects of computer networking and security with a focus on designing novel network measurement techniques to understand online information controls, network interference, and interdomain routing. She currently leads the ICLab project which is working to develop a network measurement platform specifically for online information controls. She was recently included on N2Women’s list of 10 women in networking to watch. She has received the NSF CAREER award, Google Faculty Research Award and best paper awards at the ACM Internet Measurement Conference (characterizing online aggregators), and Passive and Active Measurement Conference (characterizing interconnectivity of large content providers).",
            "website" : "https://people.cs.umass.edu/~phillipa/",
            "workshop" : false,
            "date" : "2017-06-20",
            "time_start" : "10:45:00",
            "time_end" : "12:15:00"
          },

  23 : {  "name" : "Phillipa Gill",
          "company" : "University of Massachusetts -- Amherst",
          "short_company" : "UMASS Amherst",
          "company_link" : "https://www.cics.umass.edu/",
          "pic" : "http://www3.cs.stonybrook.edu/~phillipa/img/pgill.JPG",
          "content" : "",
          "talk_title" : "Measuring And Circumventing Internet Censorship (II)",
          "talk_abstract" : "This tutorial will be split into two parts. The first half of the tutorial will cover how censorship is implemented at different layers of the protocol stack. For each layer, we will discuss how censorship is implemented, what might trigger censorship, and how we can design experiments to better understand censorship. The second half of the tutorial will focus on research going on in my research group. This will include results of recent field studies undertaken with the ICLab platform, methods to detect more subtle traffic differentiation. Time permitting, I will also discuss our recent work on AS-aware Tor clients and techniques to circumvent traffic shaping middle boxes.",
          "bio" : "Phillipa Gill is an assistant professor in the Computer Science Department at the University of Massachusetts -- Amherst. Her work focuses on many aspects of computer networking and security with a focus on designing novel network measurement techniques to understand online information controls, network interference, and interdomain routing. She currently leads the ICLab project which is working to develop a network measurement platform specifically for online information controls. She was recently included on N2Women’s list of 10 women in networking to watch. She has received the NSF CAREER award, Google Faculty Research Award and best paper awards at the ACM Internet Measurement Conference (characterizing online aggregators), and Passive and Active Measurement Conference (characterizing interconnectivity of large content providers).",
          "website" : "https://people.cs.umass.edu/~phillipa/",
          "workshop" : true,
          "date" : "2017-06-20",
          "time_start" : "13:15:00",
          "time_end" : "14:45:00"
        },

    3 : {  "name" : "Brighten Godfrey",
            "company" : "Univeristy of Illinois at Urbana-Champaign",
            "short_company" : "UIUC",
            "company_link" : "http://www.cs.illinois.edu/",
            "pic" : "http://pbg.cs.illinois.edu/me.jpg",
            "content" : "",
            "talk_title" : "Network Verification From Algorithms To Deployment (I)",
            "talk_abstract" : "Enterprise networks have become complex software artifacts weaving together interdependent devices, protocols, virtualization layers, optimizations, software bugs, and security controls. Yet communication is now critical infrastructure. How can we be sure networks are doing the right thing? The fact is, too often, they aren't. Despite enormous amounts of human time – a network or security change might spend a week in a review board in a large enterprise – outages and security vulnerabilities are common.<br>Can we prove that a network is correct?  This is the goal of the emerging area of network verification.  By applying ideas from formal methods to network infrastructure, algorithms and systems have recently been developed which can provide strong assurance that network behavior meets service availability, segmentation, and resilience intent of the network architect.  In this tutorial, we'll begin with the theoretical and algorithmic foundations of network verification, and discuss approaches that provide verification of the data plane and control plane, and how research is pushing the boundaries into verification of more complex network software.<br>Network verification blossomed into a hot area of research and quickly had impact on industry, with multiple startups, major networking companies and hyperscale cloud providers deploying verification technology today. We'll discuss key practical challenges and lessons learned, and how enterprises use these solutions in practice.  Finally, we will close with thoughts on future research directions.",
            "bio" : "P. Brighten Godfrey is an associate professor of computer science at the University of Illinois at Urbana-Champaign  and co-founder and CTO of Veriflow. He received his Ph.D. at UC Berkeley in May 2009, and his B.S. at Carnegie Mellon University in 2002. His research interests lie in the design of networked systems and algorithms. He is a winner of the ACM SIGCOMM Rising Star Award, the Sloan Research Fellowship, the UIUC Dean's Award for Excellence in Research, the National Science Foundation CAREER Award, and several best paper awards.  He was a Beckman Fellow at the UIUC Center for Advanced Study in 2014-2015, and served as program committee co-chair of ACM HotNets (2014) and the Symposium on SDN Research (2016).",
            "website" : "http://pbg.cs.illinois.edu/",
            "workshop" : true,
            "date" : "2017-06-21",
            "time_start" : "15:30:00",
            "time_end" : "16:30:00"
          },

  25 : {  "name" : "Brighten Godfrey",
          "company" : "Univeristy of Illinois at Urbana-Champaign",
          "short_company" : "UIUC",
          "company_link" : "http://www.cs.illinois.edu/",
          "pic" : "http://pbg.cs.illinois.edu/me.jpg",
          "content" : "",
          "talk_title" : "Network Verification from Algorithms to Deployment (II)",
          "talk_abstract" : "Enterprise networks have become complex software artifacts weaving together interdependent devices, protocols, virtualization layers, optimizations, software bugs, and security controls. Yet communication is now critical infrastructure. How can we be sure networks are doing the right thing? The fact is, too often, they aren't. Despite enormous amounts of human time – a network or security change might spend a week in a review board in a large enterprise – outages and security vulnerabilities are common.<br>Can we prove that a network is correct?  This is the goal of the emerging area of network verification.  By applying ideas from formal methods to network infrastructure, algorithms and systems have recently been developed which can provide strong assurance that network behavior meets service availability, segmentation, and resilience intent of the network architect.  In this tutorial, we'll begin with the theoretical and algorithmic foundations of network verification, and discuss approaches that provide verification of the data plane and control plane, and how research is pushing the boundaries into verification of more complex network software.<br>Network verification blossomed into a hot area of research and quickly had impact on industry, with multiple startups, major networking companies and hyperscale cloud providers deploying verification technology today. We'll discuss key practical challenges and lessons learned, and how enterprises use these solutions in practice.  Finally, we will close with thoughts on future research directions.",
          "bio" : "P. Brighten Godfrey is an associate professor of computer science at the University of Illinois at Urbana-Champaign  and co-founder and CTO of Veriflow. He received his Ph.D. at UC Berkeley in May 2009, and his B.S. at Carnegie Mellon University in 2002. His research interests lie in the design of networked systems and algorithms. He is a winner of the ACM SIGCOMM Rising Star Award, the Sloan Research Fellowship, the UIUC Dean's Award for Excellence in Research, the National Science Foundation CAREER Award, and several best paper awards.  He was a Beckman Fellow at the UIUC Center for Advanced Study in 2014-2015, and served as program committee co-chair of ACM HotNets (2014) and the Symposium on SDN Research (2016).",
          "website" : "http://pbg.cs.illinois.edu/",
          "workshop" : true,
          "date" : "2017-06-21",
          "time_start" : "16:40:00",
          "time_end" : "17:40:00"
        },

    4 : {  "name" : "George Danezis",
            "company" : "University College London",
            "short_company" : "UCL",
            "company_link" : "http://sec.cs.ucl.ac.uk/home/",
            "pic" : "http://www0.cs.ucl.ac.uk/staff/G.Danezis/danezisbw.jpg",
            "content" : "",
            "talk_title" : "Foiling on-line surveillance: new developments in anonymous communications and their applications (I)",
            "talk_abstract" : "The internet was built without strong privacy protections, making its use today susceptible to routine mass surveillance. Our research for the past 15 years has focused on building scalable protections for on-line communications that hide content, but also “anonymity systems” that hide the meta-data of who is talking with whom on-line. In this talk I will provide an overview of our latest results in attacking widely deployed anonymity systems, and present our latest designs, showing that such systems can be made robust at scale. Anonymity systems are relevant not only for communications privacy, but also as building blocks for private queries, social networking and private storage, and are likely to become a fundamental building block of information security in the years to come.",
            "bio" : "George Danezis is a Professor of Security and Privacy Engineering at the Department of Computer Science of University College London, and Head of the Information Security Research Group. He has been working on anonymous communications, privacy enhancing technologies (PET), and traffic analysis since 2000. He has previously been a researcher for Microsoft Research, Cambridge; a visiting fellow at K.U.Leuven (Belgium); and a research associate at the University of Cambridge (UK), where he also completed his doctoral dissertation under the supervision of Prof. Ross J. Anderson. <br> His theoretical contributions to the Privacy Technologies field include the established information theoretic and other probabilistic metrics for anonymity and pioneering the study of statistical attacks against anonymity systems. On the practical side he is one of the lead designers of the anonymous mail system Mixminion, as well as Minx, Sphinx, Drac and Hornet; he has worked on the traffic analysis of deployed protocols such as Tor.<br> His current research interests focus around secure communications, high-integrity systems to support privacy, smart grid privacy, peer-to-peer and social network security, as well as the application of machine learning techniques to security problems. He has published over 70 peer-reviewed scientific papers on these topics in international conferences and journals.",
            "website" : "http://www0.cs.ucl.ac.uk/staff/G.Danezis/",
            "workshop" : false,
            "date" : "2017-06-21",
            "time_start" : "09:30:00",
            "time_end" : "11:00:00"
          },

  24 : {  "name" : "George Danezis",
          "company" : "University College London",
          "short_company" : "UCL",
          "company_link" : "http://sec.cs.ucl.ac.uk/home/",
          "pic" : "http://www0.cs.ucl.ac.uk/staff/G.Danezis/danezisbw.jpg",
          "content" : "",
          "talk_title" : "Foiling on-line surveillance: new developments in anonymous communications and their applications (II)",
          "talk_abstract" : "The internet was built without strong privacy protections, making its use today susceptible to routine mass surveillance. Our research for the past 15 years has focused on building scalable protections for on-line communications that hide content, but also “anonymity systems” that hide the meta-data of who is talking with whom on-line. In this talk I will provide an overview of our latest results in attacking widely deployed anonymity systems, and present our latest designs, showing that such systems can be made robust at scale. Anonymity systems are relevant not only for communications privacy, but also as building blocks for private queries, social networking and private storage, and are likely to become a fundamental building block of information security in the years to come.",
          "bio" : "George Danezis is a Professor of Security and Privacy Engineering at the Department of Computer Science of University College London, and Head of the Information Security Research Group. He has been working on anonymous communications, privacy enhancing technologies (PET), and traffic analysis since 2000. He has previously been a researcher for Microsoft Research, Cambridge; a visiting fellow at K.U.Leuven (Belgium); and a research associate at the University of Cambridge (UK), where he also completed his doctoral dissertation under the supervision of Prof. Ross J. Anderson. <br> His theoretical contributions to the Privacy Technologies field include the established information theoretic and other probabilistic metrics for anonymity and pioneering the study of statistical attacks against anonymity systems. On the practical side he is one of the lead designers of the anonymous mail system Mixminion, as well as Minx, Sphinx, Drac and Hornet; he has worked on the traffic analysis of deployed protocols such as Tor.<br> His current research interests focus around secure communications, high-integrity systems to support privacy, smart grid privacy, peer-to-peer and social network security, as well as the application of machine learning techniques to security problems. He has published over 70 peer-reviewed scientific papers on these topics in international conferences and journals.",
          "website" : "http://www0.cs.ucl.ac.uk/staff/G.Danezis/",
          "workshop" : true,
          "date" : "2017-06-21",
          "time_start" : "11:15:00",
          "time_end" : "12:45:00"
        },

    5 : {  "name" : "Murat Kantarcioglu",
            "company" : "University of Texas at Dallas",
            "short_company" : "UT Dallas",
            "company_link" : "http://cs.utdallas.edu/",
            "pic" : "http://www.utdallas.edu/~muratk/sketch3.jpg",
            "content" : "",
            "talk_title" : "Adversarial Data Mining: Big Data Meets Cyber Security (I)",
            "talk_abstract" : "As more and more cyber security incident data ranging from systems logs to vulnerability scan results are collected, manually analyzing these collected data to detect important cyber security events become impossible. Hence, data mining techniques are becoming an essential tool for real-world cyber security applications. For example, a report from Gartner claims that “Information security is becoming a big data analytics problem, where massive amounts of data will be correlated, analyzed and mined for meaningful patterns”. Of course, data mining/analytics is a means to an end where the ultimate goal is to provide cyber security analysts with prioritized actionable insights derived from big data. This raises the question, can we directly apply existing techniques to cyber security applications?<br> One of the most important differences between data mining/machine learning for cyber security and many other data mining applications is the existence of malicious adversaries that continuously adapt their behavior to hide their actions and to make the data mining models ineffective. Unfortunately, traditional data mining/machine learning techniques are insufficient to handle such adversarial problems directly. The adversaries adapt to the data miner’s reactions, and data mining algorithms constructed based on a training dataset degrades quickly. To address these concerns, over the last couple of years new and novel data mining / machine learning techniques which is more resilient to such adversarial behavior are being developed in machine learning and data mining community. We believe that lessons learned as a part of this research direction would be beneficial for cyber security researchers who are increasingly applying machine learning and data mining techniques in practice.<br>To give an overview of recent developments in adversarial data mining, in this three hour long tutorial, we introduce the foundations, the techniques, and the applications of adversarial data mining to cyber security applications. We first introduce various approaches proposed in the past to defend against active adversaries, such as a minimax approach to minimize the worst case error through a zero-sum game. We then discuss a game theoretic framework to model the sequential actions of the adversary and the data miner, while both parties try to maximize their utilities. We also introduce a modified support vector machine method and a relevance vector machine method to defend against active adversaries. Intrusion detection and malware detection are two important application areas for adversarial data mining models that will be discussed in details during the tutorial. Finally, we discuss some practical guidelines on how to use adversarial data mining ideas in generic cyber security applications and how to leverage existing big data management tools for building data mining algorithms for cyber security.",
            "bio" : "Dr. Murat Kantarcioglu is a Professor of Computer Science and Director of the UTD Data Security and Privacy Lab at The University of Texas at Dallas. He holds a BS in Computer Engineering from Middle East Technical University, and MS and PhD degrees in Computer Science from Purdue University. He is recipient of an NSF CAREER award and a Purdue CERIAS Diamond Award for academic excellence. Currently, he is also a visiting scholar at Harvard's Data Privacy Lab.<br><br>Dr. Kantarcioglu's research focuses on creating technologies that can efficiently extract useful information from any data without sacrificing privacy or security. His research has been supported by awards from NSF, AFOSR, ONR, NSA, and NIH. He has published over 165 peer-reviewed papers. His work has been covered by media outlets such as Boston Globe and ABC News, among others and has received three best paper awards. He is an IEEE senior member and ACM Distinguished Scientist.",
            "website" : "http://www.utdallas.edu/~muratk/",
            "workshop" : false,
            "date" : "2017-06-18",
            "time_start" : "09:30:00",
            "time_end" : "11:00:00"
          },
  22 : {  "name" : "Murat Kantarcioglu",
          "company" : "University of Texas at Dallas",
          "short_company" : "UT Dallas",
          "company_link" : "http://cs.utdallas.edu/",
          "pic" : "http://www.utdallas.edu/~muratk/sketch3.jpg",
          "content" : "",
          "talk_title" : "Adversarial Data Mining: Big Data Meets Cyber Security (II)",
          "talk_abstract" : "As more and more cyber security incident data ranging from systems logs to vulnerability scan results are collected, manually analyzing these collected data to detect important cyber security events become impossible. Hence, data mining techniques are becoming an essential tool for real-world cyber security applications. For example, a report from Gartner claims that “Information security is becoming a big data analytics problem, where massive amounts of data will be correlated, analyzed and mined for meaningful patterns”. Of course, data mining/analytics is a means to an end where the ultimate goal is to provide cyber security analysts with prioritized actionable insights derived from big data. This raises the question, can we directly apply existing techniques to cyber security applications?<br> One of the most important differences between data mining/machine learning for cyber security and many other data mining applications is the existence of malicious adversaries that continuously adapt their behavior to hide their actions and to make the data mining models ineffective. Unfortunately, traditional data mining/machine learning techniques are insufficient to handle such adversarial problems directly. The adversaries adapt to the data miner’s reactions, and data mining algorithms constructed based on a training dataset degrades quickly. To address these concerns, over the last couple of years new and novel data mining / machine learning techniques which is more resilient to such adversarial behavior are being developed in machine learning and data mining community. We believe that lessons learned as a part of this research direction would be beneficial for cyber security researchers who are increasingly applying machine learning and data mining techniques in practice.<br>To give an overview of recent developments in adversarial data mining, in this three hour long tutorial, we introduce the foundations, the techniques, and the applications of adversarial data mining to cyber security applications. We first introduce various approaches proposed in the past to defend against active adversaries, such as a minimax approach to minimize the worst case error through a zero-sum game. We then discuss a game theoretic framework to model the sequential actions of the adversary and the data miner, while both parties try to maximize their utilities. We also introduce a modified support vector machine method and a relevance vector machine method to defend against active adversaries. Intrusion detection and malware detection are two important application areas for adversarial data mining models that will be discussed in details during the tutorial. Finally, we discuss some practical guidelines on how to use adversarial data mining ideas in generic cyber security applications and how to leverage existing big data management tools for building data mining algorithms for cyber security.",
          "bio" : "Dr. Murat Kantarcioglu is a Professor of Computer Science and Director of the UTD Data Security and Privacy Lab at The University of Texas at Dallas. He holds a BS in Computer Engineering from Middle East Technical University, and MS and PhD degrees in Computer Science from Purdue University. He is recipient of an NSF CAREER award and a Purdue CERIAS Diamond Award for academic excellence. Currently, he is also a visiting scholar at Harvard's Data Privacy Lab.<br><br>Dr. Kantarcioglu's research focuses on creating technologies that can efficiently extract useful information from any data without sacrificing privacy or security. His research has been supported by awards from NSF, AFOSR, ONR, NSA, and NIH. He has published over 165 peer-reviewed papers. His work has been covered by media outlets such as Boston Globe and ABC News, among others and has received three best paper awards. He is an IEEE senior member and ACM Distinguished Scientist.",
          "website" : "http://www.utdallas.edu/~muratk/",
          "workshop" : true,
          "date" : "2017-06-18",
          "time_start" : "11:15:00",
          "time_end" : "12:45:00"
        },

    6 : {  "name" : "Aniket Kate",
            "company" : "Purdue University",
            "short_company" : "Purdue",
            "company_link" : "https://www.purdue.edu/",
            "pic" : "https://www.cs.purdue.edu/homes/akate/images/Aniket.jpg",
            "content" : "",
            "talk_title" : " IOweYou Credit Networks: Not all permissionless payment solutions require a blockchain",
            "talk_abstract" : "IOweYou (IOU) credit networks model transitive trust (or credit) between users in a decentralized environment. They have recently seen a rapid increase of popularity due to their flexible-yet-scalable design and robustness against intrusion. They serve today as a backbone of real-world permission-less payment settlement networks (e.g., Ripple and Stellar) as well as several other weak-identity systems such as spam-resistant communication protocols and Sybil-tolerant social networks. In payment scenarios, due to their unique capability to unite emerging crypto-currencies (such as Bitcoin) and user-defined currencies with the traditional fiat currency and banking systems, several existing and new payment enterprises are entering in this space.<br>Nevertheless, this enthusiasm in the market significantly exceeds our understanding of security, privacy, and reliability of these inherently distributed systems. Currently employed ad hoc strategies to fix apparent flaws have made those systems vulnerable to bigger problems once they become lucrative targets for malicious players. In this talk, we will first define the concept of IOU credit networks, and describe some of the important credit network applications. We will then present and analyze the recent and ongoing projects to improve the credit-network security, privacy and reliability.",
            "bio" : "Dr. Aniket Kate is an Assistant Professor in the computer science department at Purdue University. Before joining Purdue in 2015, he was a faculty member and an independent research group leader at Saarland University in Germany, where he was heading the Cryptographic Systems Research Group. He completed his postdoctoral fellowship at Max Planck Institute for Software Systems (MPI-SWS), Germany in 2012, and received his PhD from the University of Waterloo, Canada in 2010. His research interests include design, implement, and analyze privacy and transparency enhancing technologies. His research integrates applied cryptography and distributed computing.",
            "website" : "https://www.cs.purdue.edu/homes/akate/",
            "workshop" : true,
            "date" : "2017-06-20",
            "time_start" : "09:00:00",
            "time_end" : "10:30:00"
          },

    7 : {  "name" : "Jonathan Katz",
            "company" : "University of Maryland",
            "short_company" : "UMD",
            "company_link" : "https://www.cs.umd.edu/",
            "pic" : "http://www.cyber.umd.edu/sites/default/files/images/faculty/katz2014.png",
            "content" : "",
            "talk_title" : "Recent Progress in Efficient, Generic Secure Computation",
            "talk_abstract" : "For decades, secure computation has offered hope for several revolutionary applications if only it could be made efficient enough. Are we there yet? This talk will survey some recent work on secure two-party and multi-party computation that has led to significant improvements in the efficiency of such protocols, perhaps (finally) bringing them to the brink of real-world applications.",
            "bio" : "Jonathan Katz is a professor of computer science at the University of Maryland, and director of the Maryland Cybersecurity Center.  His research interests lie broadly in the fields of cryptography, privacy, and the science of cybersecurity, and he is a co-author of the widely used textbook <b>Introduction to Modern Cryptography<b>, now in its second edition. Katz was a member of the DARPA Computer Science Study Group from 2009-2010, and currently serves on the steering committee for the IEEE Cybersecurity Initiative as well as on the State of Maryland Cybersecurity Council.",
            "website" : "https://www.cs.umd.edu/~jkatz/",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "14:40:00",
            "time_end" : "15:20:00"
          },

    8 : {  "name" : "Eitan Zahavi",
            "company" : "Mellanox",
            "short_company" : "Mellanox",
            "company_link" : "https://www.mellanox.com/",
            "pic" : "https://i1.rgstatic.net/ii/profile.image/AS%3A438920400904192%401481658186011_l/Eitan_Zahavi.png",
            "content" : "",
            "talk_title" : "Mellanox Research Challenges",
            "talk_abstract" : "In this presentation we will overview a few of the outstanding questions Mellanox architecture teams research. Mellanox vision to develop the next state of the art communication network, is a source for critical research problems. Mellanox, solution includes the cables, devices, boards and switch systems, OS drivers, acceleration and management software puts us in the unique position to investigate all levels of the communication stack. In my talk I will describe some of our current research topics in the physical media, like topologies and optical switching; in the link level like adaptive routing and congestion control; in the combination of hardware and software like OVS offloading; and in the Machine Learning field, like parallel data learning acceleration and inference acceleration.",
            "bio" : "Dr. Eitan Zahavi, serves as a distinguished architect in Mellanox. As a cofounder of Mellanox, he led the VLSI design methodology and tools group from 1999 to 2012 in parallel he was the main developer of the InfiniBand SDN controller 2003-2006.  Today, Eitan leads the End-to-End Network Architecture group researching network wide features. He is a co-chair of the IBTA Management working group, and actively research topologies, Congestion Control and Adaptive Routing technologies. Eitan graduated the Technion EE at 1987 and his PhD at 2015. Prior to joining Mellanox, on 1992 to 1999, served in various Chip design and Design Automation positions at Intel uP group and the Strategic CAD Lab. Eitan also teaches Logic VLSI CAD (Computer Aided Design) in the Technion for the last 4 years.",
            "website" : "https://www.mellanox.com/",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "09:30:00",
            "time_end" : "10:10:00"
          },

    9 : {  "name" : "Ankit Singla",
            "company" : "ETH Zurich",
            "short_company" : "ETH",
            "company_link" : "https://www.inf.ethz.ch/",
            "pic" : "https://people.inf.ethz.ch/asingla/img/me.jpg",
            "content" : "",
            "talk_title" : "A grand challenge for networking: a speed-of-light Internet",
            "talk_abstract" : "Retrieving even small Web objects over today’s Internet takes one to two orders of magnitude longer than the speed-of-light lower-bound. Closing this gap would not only benefit today’s applications, but also open the door to exciting new applications. Thus, we propose a grand challenge for networking: building a speed-of-light Internet. Towards addressing this goal, we begin by investigating the Internet’s latency inflation with extensive measurements. We find that while protocol overheads are indeed important, infrastructural inefficiencies are also significant—without these inefficiencies, small object fetches would be 3x faster. We propose two approaches to improve latency at the lower layers: (a) a surprisingly low-cost approach to building a nearly speed-of-light Internet infrastructure using microwave networking; and (b) an overlay-based approach attempting to best utilize existing fiber. While these proposals provide substantially lower latency than today’s Internet, they are also bandwidth-constrained. Therefore, we explore their potential with a variety of applications, showing significant performance gains even with the use of their limited bandwidth.",
            "bio" : "Ankit Singla joined ETH Zürich as an Assistant Professor in the Department of Computer Science in 2016. Prior to that, he obtained his PhD in Computer Science at the University of Illinois at Urbana-Champaign in 2015, and his Bachelors in Technology (Computer Science) at IIT Bombay, India, in 2008. He is a winner of the 2012 Google PhD Fellowship, and the instructor of a massive open online course on Cloud Networking.",
            "website" : "https://people.inf.ethz.ch/asingla/",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "11:05:00",
            "time_end" : "11:45:00"
          },

  10 : {  "name" : "Alon Amid",
          "company" : "Berkeley",
          "short_company" : "Berkeley",
          "company_link" : "https://eecs.berkeley.edu/",
          "pic" : "imgs/people/AlonAmid.jpg",
          "content" : "",
          "talk_title" : "Designing a RISC-V Graph Processor - Challenges and Insights",
          "talk_abstract" : "Graph processing has been a recent topic of interest in the high performance computing and systems community in the context of data analytics. Graph processing presents difficulties in data-level parallelism and data-layout, which depend on the graph structure. In this talk, I will describe our current process of designing a RISC-V processor, with the purpose of enhancing graph analytics. I will describe insights discovered during this design process, the hardware challenges of graph processing, and what role do high level frameworks play in this design.",
          "bio" : "Alon is a first year PhD student in the Berkeley EECS department. He received his B.Sc in Electrical Engineering from Technion in 2016. His general research interests include computer systems, computer architecture and digital hardware design. Specifically, he is currently interested in high-performance integrated software-hardware systems in the contexts of domain-specific accelerators and energy efficiency.",
          "website" : "#",
          "workshop" : false,
          "skip": true,
          "date" : "2017-06-22",
          "time_start" : "13:25:00",
          "time_end" : "13:40:00"
        },

  12 : {  "name" : "Simon Kassing",
          "company" : "ETH Zurich",
          "short_company" : "ETH",
          "company_link" : "https://www.inf.ethz.ch/",
          "pic" : "imgs/people/SimonKassing.png",
          "content" : "",
          "talk_title" : "TBD",
          "talk_abstract" : "TBD",
          "bio" : "TBD",
          "website" : "#",
          "workshop" : false,
          "skip": true,
          "date" : "2017-06-22",
          "time_start" : "14:10:00",
          "time_end" : "14:25:00"
        },

    11 : {  "name" : "Adi Fuchs",
            "company" : "Princeton",
            "short_company" : "Princeton",
            "company_link" : "http://ee.princeton.edu/",
            "pic" : "http://www.princeton.edu/~adif/images/top.jpg",
            "content" : "",
            "talk_title" : "Finding the Next Curves: Towards a Scalable Future for Specialized Architectures",
            "talk_abstract" : "The end of CMOS transistors scaling marks a new era for modern computer systems. As the gains from traditional general-purpose processors diminish, researchers explore the new avenues of domain-specific computing. The premise of domain-specific computing is that by co-optimizing software and specialized hardware accelerators, it is possible to achieve higher performance per power rates. In contrast to technology scaling, specialization gains are not sustainably scalable, as there is a limit to the number of ways to map a computation problem to hardware under a fixed budget. Since hardware accelerators are also implemented using CMOS transistors, the gains from specialization will also diminish in the farther future, giving rise to a “specialization wall”. We explore the integration of emerging memory technologies with specialized hardware accelerators to eliminate redundant computations and essentially trade non-scaling CMOS transistors for scaling memory technology. We evaluated our new architecture for different data center workloads. Our results show that, compare to highly-optimized accelerators, we achieve an average speedup of 3.5x, save on average 43% in energy, and save on average 57% in energy-delay product.",
            "bio" : "Adi Fuchs is a PhD Candidate in the EE Department at Princeton University working on novel computer architectures. His research explores the integration of new scaling technologies with existing computer systems and domain-specific accelerators. He earned his BSc and MSc degrees, cum laude and summa cum laude, respectively, both from the EE Department at Technion – Israel Institute of Technology.",
            "website" : "http://www.princeton.edu/~adif/",
            "workshop" : false,
            "skip": true,
            "date" : "2017-06-22",
            "time_start" : "13:40:00",
            "time_end" : "13:55:00"
          },

    13 : {  "name" : "Dmitry Kogan",
            "company" : "Stanford",
            "short_company" : "Stanford",
            "company_link" : "https://www-cs.stanford.edu/",
            "pic" : "imgs/people/DimaKogan.jpg",
            "content" : "",
            "talk_title" : "SSH Connections from Semi-Trusted Clients",
            "talk_abstract" : "The Secure Shell (SSH) protocol is widely used for remote login as well as for providing a secure channel for other network services. User authentication often uses public-key cryptography, in which case the client uses the user’s private key to generate a signature, which is then verified by the server against a stored list of authorized public keys. In practice, a need often arises to initiate SSH sessions from semi-trusted client machines (for example to access a private remote git repository from a client running in the cloud). In such cases, the client requires the user’s private key to authenticate itself to the server, yet the user wants to refrain from disclosing her private key to the semi-trusted client. Existing solutions do not provide the desired level of security (as in the case of the widely used SSH-agent forwarding) and often come with a cost of complexity for the user.<br>In this talk, I will describe our solution to this problem, an SSH gateway, which handles authentication on behalf of semi-secure clients, subject either to direct consent from the user or a predefined policy. Our gateway also supports fine-grained control over individual commands performed on the SSH connection. In the core of our system is a novel technique for handing-off established SSH connections, which allows us to avoid the bandwidth and latency costs of proxying the entire connection. Our system has the practical advantage of supporting unmodified SSH servers.<br>Joint work with David Mazieres, Henri Stern, and Keith Winstein.",
            "bio" : "Dmitry Kogan is a first year PhD student in Stanford University working with Dan Boneh, Keith Winstein and Matei Zaharia, focusing on security and privacy. Dmitry holds an MSc in computer science from the Weizmann Institute, and a BSc in mathematics, physics and computer science from the Hebrew University, where he graduated from the “Talpiot” program. He has previously worked in the Government of Israel, as well as in Google, where he focused on data analytics and account security",
            "website" : "#",
            "workshop" : false,
            "skip": true,
            "date" : "2017-06-22",
            "time_start" : "13:55:00",
            "time_end" : "14:10:00"
    },

    14 : {  "name" : "Brighten Godfrey",
            "company" : "University of Illinois at Urbana-Champaign",
            "short_company" : "UIUC",
            "company_link" : "http://cs.illinois.edu/",
            "pic" : "http://pbg.cs.illinois.edu/me.jpg",
            "content" : "",
            "talk_title" : "Micro Load Balancing in Data Centers",
            "talk_abstract" : "A key question for open networking is what functionality should be in network hardware, vs. at the edge or in a controller.  In the sphere of data center networks, the trend towards simpler network fabric strips most network functionality, including load balancing capabilities, out of the network core and pushes them to the edge. We investigate a different direction of incorporating minimal load balancing intelligence into the network fabric and show that this slightly smarter fabric significantly enhances performance. We provide a very simple in-network load balancing scheduling algorithm called DRILL which is purely local to each switch. DRILL leverages local load sensing and randomization concepts to distribute load among multiple paths. Simulations show that this new “micro load balancing” approach can achieve dramatically lower flow completion time and queuing variability than past designs.  This is joint work with Soudeh Ghorbani, P. Brighten Godfrey, Yashar Ganjali, and Amin Firoozshahian.",
            "bio" : "P. Brighten Godfrey is an associate professor of computer science at the University of Illinois at Urbana-Champaign  and co-founder and CTO of Veriflow. He received his Ph.D. at UC Berkeley in May 2009, and his B.S. at Carnegie Mellon University in 2002. His research interests lie in the design of networked systems and algorithms. He is a winner of the ACM SIGCOMM Rising Star Award, the Sloan Research Fellowship, the UIUC Dean's Award for Excellence in Research, the National Science Foundation CAREER Award, and several best paper awards.  He was a Beckman Fellow at the UIUC Center for Advanced Study in 2014-2015, and served as program committee co-chair of ACM HotNets (2014) and the Symposium on SDN Research (2016).",
            "website" : "http://pbg.cs.illinois.edu/",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "10:10:00",
            "time_end" : "10:50:00"
          },

    15 : {  "name" : "Assaf Eisenman",
            "company" : "Stanford",
            "short_company" : "Stanford",
            "company_link" : "https://www-cs.stanford.edu/",
            "pic" : "imgs/people/AssafEisenman.png",
            "content" : "",
            "talk_title" : "Flashield: a Key-value Cache that Minimizes Writes to Flash",
            "talk_abstract" : "As its price per bit drops, SSD is increasingly becoming the default storage medium for cloud application databases. However, it has not become the preferred storage medium for key-value caches, even though SSD offers more than 10x lower price per bit and sufficient performance compared to DRAM. This is because key-value caches need to frequently insert, update and evict small objects. This causes excessive writes and erasures on flash storage, since flash only supports writes and erasures of large chunks of data. These excessive writes and erasures significantly shorten the lifetime of flash, rendering it impractical to use for key-value caches. Flashield is a hybrid key-value cache that uses DRAM as a “filter” to minimize writes to SSD. It performs light-weight machine learning profiling to predict which objects are likely to be read frequently before getting updated; In order to efficiently utilize the cache’s available memory, Flashield leverages a novel in-memory index for the variable-sized objects stored on flash.",
            "bio" : "Assaf Eisenman is a PhD student at Stanford University, advised by Sachin Katti. His research interests are in cloud computing and storage systems. Assaf has done research at Facebook and Hewlett-Packard Labs, and was previously a CPU Performance Architect at Intel. He graduated cum laude with BSc in Computer Engineering from the Technion, and earned his MS in Electrical Engineering from Stanford University",
            "website" : "#",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "15:20:00",
            "time_end" : "16:00:00"
          },

    16 : {  "name" : "Keith Winstein",
            "company" : "Stanford",
            "short_company" : "Stanford",
            "company_link" : "https://cs.stanford.edu/",
            "pic" : "https://cs.stanford.edu/~keithw/www/keithw-2013.jpeg",
            "content" : "",
            "talk_title" : "And the networks and the codecs shall lie down together...",
            "talk_abstract" : "Video and pictures are the biggest things on the Internet, but codecs and network systems treat each other as black boxes. My colleagues and I have realized substantial performance improvements by making friends with, and refactoring the abstractions we use to talk with, our codec brethren. I'll discuss three systems that use ideas from functional programming to show the benefits of a richer interface between the codec and the network. The first (ExCamera) achieves low-latency, massively parallel video encoding by formulating the encoder in explicit state-passing style, allowing us to break the job up into tiny tasks and then run them on thousands of threads on AWS Lambda. The second (Salsify) uses the same ideas for real-time videoconferencing: by putting transport in the driver`s seat and \"late-binding\" encoding decisions, our system achieves better video quality and lower delay than Skype, Facetime, Hangouts, and WebRTC. The third (Lepton) uses an \"explicit-state\" video codec, repurposed to still images, to build a distributed compression engine that can handle small -- and independent -- segments of a JPEG image. We used this to compress over 200 petabytes of images at Dropbox, turning them into 154 petabytes and saving the company a lot of money.",
            "bio" : "Keith Winstein is assistant professor of computer science and, by courtesy, of law at Stanford University.",
            "website" : "https://cs.stanford.edu/~keithw/",
            "workshop" : false,
            "date" : "2017-06-22",
            "time_start" : "16:55:00",
            "time_end" : "17:35:00"
          },


    17 : {  "name" : "Yotam Harchol",
            "company" : "VMware Research",
            "short_company" : "VMware Research",
            "company_link" : "https://research.vmware.com/",
            "pic" : "https://static.wixstatic.com/media/e3de9c_d736221fefb74f8db1208f19bc5d20e3.jpg/v1/fill/w_252,h_288,al_c,lg_1,q_80/e3de9c_d736221fefb74f8db1208f19bc5d20e3.webp",
            "content" : "",
            "talk_title" : " SDN, NFV, and Network Security",
            "talk_abstract" : "Software-defined networking (SDN) and network function virtualization (NFV) are two major paradigm shifts in the networking field, aimed at increasing network flexibility and programmability, and reducing operation cost. These new technologies enable several new security applications, but they also raise several interesting vulnerabilities and security questions.<br><br>We will begin with an introduction to SDN and NFV. Then, we will review recent research on network security with regard to SDN and NFV. We will cover prominent security applications of SDN and NFV, vulnerabilities, and verification techniques. We will also discuss new frameworks for security management in SDN environments.",
            "bio" : "Yotam Harchol is a postdoctoral researcher with VMware Research. He completed his PhD studies at the Hebrew University of Jerusalem, Israel. His research is focused on improved performance and security of network functions and middleboxes, and on applied cryptography for network security.",
            "website" : "http://www.yotamharchol.com/",
            "workshop" : false,
            "date" : "2017-06-21",
            "time_start" : "13:45:00",
            "time_end" : "15:15:00"
          },

  18 : {  "name" : "Yossi Gilad",
          "company" : "Boston University and MIT",
          "short_company" : "BU and MIT",
          "company_link" : "http://www.mit.edu/",
          "pic" : "http://www.mit.edu/~yossigi/yossig.jpg",
          "content" : "",
          "talk_title" : "Algorand: Scaling Byzantine Agreements for Cryptocurrencies",
          "talk_abstract" : "Today's cryptocurrencies are often slow and allow temporary forks, where some users in the network have a different view of the state of transactions than others, and therefore require a long time to confirm transactions. Moreover, they allow attackers to exploit cuts in the network (e.g., by disconnecting users) to create such forks. We present a new cryptocurrency system,  Algorand, that (1) is not computationally intensive and can run on commodity devices by anyone (``one-class'' of users), (2) scales efficiently to many users, (3) quickly confirms transactions, and (4) does not generate forks despite many malicious users and cuts in the network.  Algorand uses a novel mechanism based on Verifiable Random Functions that allows users to privately check whether they should participate in the Byzantine agreement protocol over the next set of transactions, and attach a proof of their selection to their message. In the Byzantine agreement protocol, users do not keep any private state. This allows Algorand to replace participants after they send a message, in order to mitigate attacks on selected users.",
          "bio" : "Yossi Gilad is a postdoctoral fellow at Boston University (BU) and Massachusetts Institute of Technology (MIT). His research interests lie in network security, including identifying vulnerabilities and mitigating attacks on today’s networking mechanisms/protocols,  designing and building practical and secure networked systems, and privacy and anonymity-preserving systems.  Prior to joining BU/MIT, he was a postdoctoral researcher at the Hebrew University of Jerusalem and a researcher at IBM Research in Israel. ",
          "website" : "http://www.mit.edu/~yossigi/",
          "workshop" : false,
          "date" : "2017-06-18",
          "time_start" : "16:40:00",
          "time_end" : "17:40:00"
        },

  19 : {  "name" : "Yonatan Sompolinsky",
          "company" : "Hebrew University",
          "short_company" : "HUJI",
          "company_link" : "http://www.cs.huji.ac.il/",
          "pic" : "http://www.cs.huji.ac.il/~yoni_sompo/homepage2.jpg",
          "content" : "",
          "talk_title" : "The scalability challenge of cryptocurrency protocols: limitations of Nakamoto's chain, and alternative DAG-structured ledgers.",
          "talk_abstract" : "A growing body of research on Bitcoin and other permissionless cryptocurrencies that utilize Nakamoto’s blockchain has shown that they do not easily scale to process a high throughput of transactions, or to quickly approve individual transactions; blocks must be kept small, and their creation rates must be kept low in order to allow nodes to reach consensus securely. As of today, Bitcoin processes a mere 3-7 transactions per second, and transaction confirmation takes at least several minutes.<br><br>In this talk we will discuss one approach to this problem: replacing Nakamoto's Consensus, which resolves conflicts between blocks according to the Longest-chain rule, by alternative more sophisticated protocols. In particular, blocks can be organized in a growing (potentially massive) Directed Acyclic Graph, rather than be confined to extend a single chain. The main advantage gained by these schemes is the utilization of all (or most of) the blocks created in the system, even those not created in one consistent chain, and harvesting the efforts of all (honest) miners. Consequently, frequent and large blocks can be created without degrading the system's security and performance, thereby alleviating the scalability barrier.<br><br>Some of these protocols are already implemented in working blockchain projects. In the talk I will describe several protocols (e.g., Inclusive, GHOST, SPECTRE, BitcoinNG), and discuss the pros and cons of each and the trade-offs they impose.",
          "bio" : "Yonatan is a PhD student at the Rachel and Selim Benin School of Computer Science and Engineering at the Faculty of Science, the Hebrew University of Jerusalem, under the supervision of Aviv Zohar. He obtained his M.Sc. in Computer Science, and his B.Sc. in Mathematics, from the Hebrew University.",
          "website" : "http://www.cs.huji.ac.il/~yoni_sompo/",
          "workshop" : false,
          "date" : "2017-06-18",
          "time_start" : "15:30:00",
          "time_end" : "16:30:00"
        },

  20 : {  "name" : "Noa Zilberman",
          "company" : "University of Cambridge",
          "short_company" : "Cambridge",
          "company_link" : "http://www.cl.cam.ac.uk/",
          "pic" : "http://www.cl.cam.ac.uk/~nz247/view.jpg",
          "content" : "",
          "talk_title" : "Systems For Big Data Applications: A Networking Perspective",
          "talk_abstract" : "Big data applications increasingly dominate every aspect of our lives. Healthcare, finance and transportation are only a partial list of a growing number of fields where big data is being used. These big data applications, often running in the cloud, require significant computing resources and drive the research of high-performance systems. Despite the importance of the network in these systems, the understanding of networking effects on applications performance is still in its infancy. This talk addresses this challenge, providing a networking perspective on big data applications. The first part of the talk explores the effect of one network property, latency, on the performance of big data application and provides a breakdown of the basic latency components within a high performance system. It also introduces new monitoring and traffic control tools that enable creating detailed network profiles of different big data applications. <br>The second part of the talk explores different solutions that allow scaling the performance of systems for big data applications, including new networked-systems architectures and new frameworks that allow rapid prototyping of networking services on hardware using high level languages, such as C# and P4.",
          "bio" : "Noa Zilberman is a Leverhulme Early Career Fellow at the University of Cambridge Computer Laboratory. Her research focuses on high performance systems, combining aspects of computer networks, computer architecture and systems. She is the Chief Architect of the NetFPGA-SUME project, an open source platform for rapid prototyping of high-performance networking devices. In parallel, she is the PI on projects studying systems for big-data applications and rack-scale optimized fabrics. She has over 15 years of industrial experience, in design, management and architecture roles. In her last role before joining Cambridge, she was a Senior Principal Chip Architect at Broadcom's Network Switching group. Noa received her BSC, MSc and PhD in Electrical Engineering from Tel-Aviv University, is a senior member of IEEE and a member of ACM, Usenix and BCS. ",
          "website" : "http://www.cl.cam.ac.uk/~nz247/",
          "workshop" : false,
          "date" : "2017-06-22",
          "time_start" : "11:45:00",
          "time_end" : "12:25:00"
        },

  21 : {  "name" : "Aniket Kate",
          "company" : "Purdue University",
          "short_company" : "Purdue",
          "company_link" : "https://www.purdue.edu/",
          "pic" : "https://www.cs.purdue.edu/homes/akate/images/Aniket.jpg",
          "content" : "",
          "talk_title" : " Blockchain Privacy: Challenges, Solutions, and Unresolved Issues",
          "talk_abstract" : "The hope that cryptography and decentralization might together ensure robust user privacy was among the strongest drivers of early success of Bitcoin's blockchain technology. A desire for privacy still permeates the growing blockchain user base today. Nevertheless, due to the inherent public nature of most blockchain ledgers, users' privacy is severely restricted, and a few deanonymization attacks have been reported so far. Although several privacy-enhancing technologies have been proposed to solve this issues, and a few have been implemented, some important challenges still remain to be resolved.<br>In this tutorial, we discuss privacy challenges, promising solutions, and unresolved privacy issues with the blockchain technology. In particular, we study prominent privacy attacks on Bitcoin and other blockchain systems, we analyze the existing privacy solution ranging from Coinjoin to Zerocash, and finally we describe important unresolved challenges towards publishing and retrieving transactions privately.",
          "bio" : "Dr. Aniket Kate is an Assistant Professor in the computer science department at Purdue University. Before joining Purdue in 2015, he was a faculty member and an independent research group leader at Saarland University in Germany, where he was heading the Cryptographic Systems Research Group. He completed his postdoctoral fellowship at Max Planck Institute for Software Systems (MPI-SWS), Germany in 2012, and received his PhD from the University of Waterloo, Canada in 2010. His research interests include design, implement, and analyze privacy and transparency enhancing technologies. His research integrates applied cryptography and distributed computing.",
          "website" : "https://www.cs.purdue.edu/homes/akate/",
          "workshop" : false,
          "date" : "2017-06-18",
          "time_start" : "13:45:00",
          "time_end" : "15:15:00"
        },
};

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var sortObjectByKey = function(obj, isWorkshop){
    var keys = [];
    var sorted_obj = {};

    for(var key in obj){
        if(obj.hasOwnProperty(key)){

           if(obj[key].workshop == isWorkshop){
             console.log(obj[key]);
              name_arr = obj[key].name.split(" ");
              // keys.push([name_arr[name_arr.length-1], key]);
              keys.push([obj[key].date+"T"+obj[key].time_start, key]);
            }
        }
    }
    console.log(keys);
    // sort keys
    keys.sort();
    var ind = 1;
    // create new array based on Sorted Keys
    jQuery.each(keys, function(i, key){
        sorted_obj[ind] = obj[key[1]];
        ind += 1;
    });
    return sorted_obj;
};

function get_abstracts_html(isWorkshop){
  var sorted_data = sortObjectByKey(data, isWorkshop);
  var num_participants = Object.size(sorted_data);
  // console.log(num_participants)
  total_html = "";
  for(par = 1 ; par <= num_participants ; par++){
    var row_html = "<div class=\"row\">";
    row_html += "<h3 class=\"col s12\"><b>" + sorted_data[par].talk_title + "</b></h3>";
    row_html += "<h4 class=\"col s12\">" + sorted_data[par].name + "</h4>" ;

    row_html += "<h5 class=\"col s12\">" + sorted_data[par].talk_abstract +  "</h5>";
    // <div class="col s6">This div is 6-columns wide</div>
    // <div class="col s6">This div is 6-columns wide</div>
    row_html += "</div><div class=\"divider white\"></div>";
    total_html += row_html;
  }
  return total_html;
}

function get_people_html(isWorkshop){
  var sorted_data = sortObjectByKey(data, isWorkshop);
  var num_participants = Object.size(sorted_data);
  var people_per_row = 3;
  var num_rows = num_participants/people_per_row;
  total_html = "";

  for(row=0; row< num_rows ; row++){
    var row_html = "<div class=\"row center\">";
    for(col = 1; col <= people_per_row ; col++){
      if(row*people_per_row + col > num_participants){
        continue;
      }
      row_html +=
        "<div class=\"col s3 offset-s1\">"+
        "<div class=\"card\">"+
          "<div class=\"card-image\">"+
            "<img src=\"" + sorted_data[row*people_per_row + col].pic +  "\" width=\"200\" height=\"280\">"+
              // "<span class=\"card-title\">" + sorted_data[row*people_per_row + col].name +  "</span>"+
            "</div>"+
            "<div class=\"card-content\">"+
              "<p>" + sorted_data[row*people_per_row + col].name + "</p>" +
              "<p><a href=\""+ sorted_data[row*people_per_row + col].company_link +"\">" + sorted_data[row*people_per_row + col].company + "</a></p>"+
            "</div>"+
            "<div class=\"center card-action\">"+
              "<a href=\"" + sorted_data[row*people_per_row + col].website +  "\">Website</a>"+
            "</div>"+
          "</div>"+
        "</div>";
    }
    row_html += "</div>"
    total_html += row_html;
  }
  return total_html;
}

function set_calendar(){
  events = [
    {
      title: 'Coffee Break',
      start: '2017-06-18T11:00:00',
      end: '2017-06-18T11:15:00',
    },
    {
      title: 'Lunch',
      start: '2017-06-18T12:45:00',
      end: '2017-06-18T13:45:00',
    },
    {
      title: 'Coffee Break',
      start: '2017-06-18T15:15:00',
      end: '2017-06-18T15:30:00',
    },
    {
      title: 'Coffee Break',
      start: '2017-06-18T16:30:00',
      end: '2017-06-18T16:40:00',
    },

    {
      title: 'Coffee Break',
      start: '2017-06-20T10:30:00',
      end: '2017-06-20T10:45:00',
    },
    {
      title: 'Lunch',
      start: '2017-06-20T12:15:00',
      end: '2017-06-20T13:15:00',
    },

    {
      title: 'Coffee Break',
      start: '2017-06-22T10:50:00',
      end: '2017-06-22T11:05:00',
    },
    {
      title: 'Lunch',
      start: '2017-06-22T12:25:00',
      end: '2017-06-22T13:25:00',
    },
    {
      title: 'Coffee Break',
      start: '2017-06-22T14:25:00',
      end: '2017-06-22T14:40:00',
    },
    {
      title: 'Coffee Break',
      start: '2017-06-22T16:00:00',
      end: '2017-06-22T16:15:00',
    },

    {
      title: 'Coffee Break',
      start: '2017-06-21T11:00:00',
      end: '2017-06-21T11:15:00',
    },
    {
      title: 'Lunch',
      start: '2017-06-21T12:45:00',
      end: '2017-06-21T13:45:00',
    },
    {
      title: 'Coffee Break',
      start: '2017-06-21T15:15:00',
      end: '2017-06-21T15:30:00',
    },
    {
      title: 'Coffee Break',
      start: '2017-06-21T16:30:00',
      end: '2017-06-21T16:40:00',
    },
    // {
    //   title: 'Sightseeing in Jerusalem',
    //   start: '2017-06-20T15:00:00',
    //   end: '2017-06-20T18:00:00',
    // },
  ]

  student_evnet_obj = {
    title: 'Student Spotlight:\nAlon Amid (Berkeley), Adi Fuchs (Princeton), Simon Kassing (ETH), Dima Kogan (Stanford)',
    start: '2017-06-22T13:25:00',
    end: '2017-06-22T14:25:00',
    objs: []
  };

  // add student spotlight (instead of small talks)
  for(var key in data){
      if(data.hasOwnProperty(key)){
        if(!data[key].hasOwnProperty("skip")){
          events.push({
            title : data[key].name + ", " + data[key].short_company +":\n" + toTitleCase(data[key].talk_title),
            obj : data[key],
            start: data[key].date + "T" + data[key].time_start,
            end: data[key].date + "T" + data[key].time_end
          });
        } else{
          student_evnet_obj.objs.push(data[key]);
        }
    }
  }

  events.push(student_evnet_obj)

  $('#calendar').fullCalendar({
      header : false,
      footer:false,
      slotEventOverlap:false,
      hiddenDays: [ 1, 5, 6 ],
      defaultView: 'agendaWeek',
      defaultDate: '2017-06-18',
      minTime: "08:30:00",
      maxTime: "18:00:00",
      slotDuration: "00:10:00",
      navLinks: false, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: events,
      eventClick: function(calEvent, jsEvent, view) {
        if(calEvent.hasOwnProperty("obj")){
          $('#myModal #myModalTitle').html(toTitleCase(calEvent.obj.talk_title));
          $('#myModal #myModalBody').html(calEvent.obj.talk_abstract);
          $('#myModal #myModalSpeaker').html("Speaker: " + calEvent.obj.name + ", " + calEvent.obj.company);
          $('#myModal #myModalBio').html("<b>Bio:</b> " + calEvent.obj.bio);
          $("#myModal").modal('open');
        } else if(calEvent.hasOwnProperty("objs")){
          for(var obj_id in calEvent.objs){
            obj = calEvent.objs[obj_id]
            $('#myMultiModal #myModalTitle'+obj_id).html(obj.talk_title);
            $('#myMultiModal #myModalBody'+obj_id).html(obj.talk_abstract);
            $('#myMultiModal #myModalSpeaker'+obj_id).html("Speaker: " + obj.name + ", " + obj.company);
            $('#myMultiModal #myModalBio'+obj_id).html("<b>Bio:</b> " + obj.bio);
          }
          $("#myMultiModal").modal('open');
        }
        // alert('Event: ' + calEvent.title);
        // alert('View: ' + view.name);
      }
  });
}

$(document).ready(function(){
  // $("#abstracts").append(get_abstracts_html(false));
  $('.modal').modal();
  $("#people_2017").append(get_people_html(false));
  set_calendar();
  // $('.fc-time-grid-event').attr('data-toggle', 'modal');
  // $('.fc-time-grid-event').attr('data-target', '#myModal');
  // $('#myModal').on('show.bs.modal', function(e) {
  //
  //   alert(e)
  // });
});
